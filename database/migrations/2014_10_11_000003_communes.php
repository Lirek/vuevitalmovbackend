<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Communes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunas', function (Blueprint $table) {
            $table->id();            
            $table->text('cut')->nullable();            
            $table->text('latitud')->nullable();
            $table->text('longitud')->nullable();
            $table->text('comuna')->nullable();            
            $table->unsignedBigInteger('provincia_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();     
            $table->foreign('provincia_id')->references('id')->on('provincias'); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
