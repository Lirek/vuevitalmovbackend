<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserIndicators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_indicators', function (Blueprint $table) {            
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->unsignedBigInteger('objectives_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);
            $table->foreign('objectives_id')->references('id')->on('objectives');
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('status', ['Aprobado', 'Denegado','En Proceso'])->nullable()->default('En Proceso');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
