<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivitiesObjectives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objectives_activities', function (Blueprint $table) {            
            $table->unsignedBigInteger('activities_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('objectives_id')->nullable()->default(NULL);
            $table->foreign('activities_id')->references('id')->on('activities');
            $table->foreign('objectives_id')->references('id')->on('objectives');
            $table->primary('activities_id','objectives_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
