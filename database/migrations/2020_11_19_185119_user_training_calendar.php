<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserTrainingCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_training_calendar', function (Blueprint $table) {            
            $table->increments('id');
            $table->unsignedBigInteger('lesson_id')->nullable()->default(NULL);            
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);
            $table->date('atendance_date')->nullable()->default(NULL); 
            $table->text('atendance')->nullable()->default(NULL);          
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lesson_id')->references('id')->on('lessons');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
