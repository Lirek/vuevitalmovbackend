<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Provinces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->id();
            $table->text('provincia')->nullable();
            $table->unsignedBigInteger('region_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();     
            $table->foreign('region_id')->references('id')->on('regiones'); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
