<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {            
            $table->increments('id');
            $table->string('title');
            $table->text('image')->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);
            $table->enum('type_events_own', ['Privado', 'Publico','Particular'])->nullable();
            $table->enum('type_events', ['Presencial','Virtual'])->nullable();
            $table->enum('frecuency', ['Mensual', 'Semanal', 'Diario','Unico'])->nullable();
            $table->text('duration')->nullable()->default(NULL);
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('location_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('location_id')->references('id')->on('locations');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
