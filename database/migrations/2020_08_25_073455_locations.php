<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Locations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable()->default(NULL);            
            $table->text('description')->nullable()->default(NULL);
            $table->text('lat')->nullable()->default(NULL);
            $table->text('lon')->nullable()->default(NULL);
            $table->text('addres')->nullable()->default(NULL);
            $table->text('avatar')->nullable()->default(NULL);
            $table->enum('type_owner', ['Privada', 'Publica','Particular'])->nullable();
            $table->enum('type_location', ['Tienda', 'Locacion','Gymnasio'])->nullable();
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('comuna_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('comuna_id')->references('id')->on('comunas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
