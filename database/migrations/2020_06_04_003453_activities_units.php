<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActivitiesUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_activities', function (Blueprint $table) {            
            $table->unsignedBigInteger('activities_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('units_id')->nullable()->default(NULL);
            $table->foreign('activities_id')->references('id')->on('activities');
            $table->foreign('units_id')->references('id')->on('units');
            $table->primary('activities_id','units_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
