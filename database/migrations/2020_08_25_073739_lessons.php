<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->text('description')->nullable()->default(NULL);            
            $table->enum('type_training', ['Presencial','Virtual'])->nullable();
            $table->unsignedBigInteger('order')->nullable()->default(NULL);
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('location_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('lesson_plan_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lesson_plan_id')->references('id')->on('lesson_plan'); 
            $table->foreign('location_id')->references('id')->on('locations');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
