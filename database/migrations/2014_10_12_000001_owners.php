<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Owners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners-directors', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->text('ruc')->nullable();
            $table->text('address')->nullable();
            $table->unsignedBigInteger('institute_id')->nullable()->default(NULL);
            $table->foreign('institute_id')->references('id')->on('institute');                        
            $table->timestamps();
            $table->softDeletes();                  
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
