<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Clases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('teacher_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('course_id')->nullable()->default(NULL);
            $table->json('class_log')->nullable()->default(NULL);
            $table->float('incidencies')->nullable()->default(NULL);
            $table->float('sastisfaction')->nullable()->default(NULL);
            $table->float('level')->nullable()->default(NULL);
            $table->timestamp('start')->nullable()->default(NULL);
            $table->timestamp('end')->nullable()->default(NULL);
            $table->foreign('teacher_id')->references('id')->on('users');
            $table->foreign('course_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
