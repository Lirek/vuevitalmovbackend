<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UnitsIndicators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicators_units', function (Blueprint $table) {            
            $table->unsignedBigInteger('unit_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('indicator_id')->nullable()->default(NULL);
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('indicator_id')->references('id')->on('indicators');
            $table->primary('unit_id','indicator_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
