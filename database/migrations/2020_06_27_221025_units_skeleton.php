<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UnitsSkeleton extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_skeletons', function (Blueprint $table) {            
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->json('unit_json')->nullable();
            $table->unsignedBigInteger('lvl_id')->nullable()->default(NULL);
            $table->foreign('lvl_id')->references('id')->on('courses_levels');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
