<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Exersice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);
            $table->text('file')->nullable()->default(NULL);         
            $table->enum('type_training', ['Movilidad','Auto Cargas','Cargas Externas','Aerobicos','Core'])->nullable();   
            $table->enum('focus_training', ['Tren Superior','Tren Inferior','Multi Articular','Core'])->nullable();   
            $table->unsignedBigInteger('user_id')->nullable()->default(NULL);            
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');                         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
