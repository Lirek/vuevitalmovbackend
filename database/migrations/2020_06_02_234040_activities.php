<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Activities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);
            $table->float('rate')->nullable()->default(NULL);
            $table->unsignedBigInteger('axis_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();            
            $table->foreign('axis_id')->references('id')->on('axis_objectives');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
