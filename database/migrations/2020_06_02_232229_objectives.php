<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Objectives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objectives', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);
            $table->boolean('priority')->nullable()->default(false);
            $table->enum('status', ['Vigente', 'Desusado'])->nullable()->default('Vigente');
            $table->unsignedBigInteger('axis_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('course_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();            
            $table->foreign('axis_id')->references('id')->on('axis_objectives');
            $table->foreign('course_id')->references('id')->on('courses_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
