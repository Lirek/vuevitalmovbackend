<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LessonsExercise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_exercise', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lesson_id')->nullable()->default(NULL);            
            $table->unsignedBigInteger('exersice_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('series')->nullable()->default(NULL);
            $table->text('rest_time_s')->nullable()->default(NULL);
            $table->unsignedBigInteger('reps')->nullable()->default(NULL);
            $table->text('rest_time_r')->nullable()->default(NULL);            
            $table->text('time')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('lesson_id')->references('id')->on('lessons');
            $table->foreign('exersice_id')->references('id')->on('exercise');                         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
