<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Courses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->unsignedBigInteger('plan_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->unsignedBigInteger('level_id')->nullable();    
            $table->foreign('teacher_id')->references('id')->on('users');
            $table->foreign('plan_id')->references('id')->on('planings_class');
            $table->foreign('level_id')->references('id')->on('courses_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
