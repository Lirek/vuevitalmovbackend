<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Actitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actitudes', function (Blueprint $table) {            
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('lvl_id')->nullable()->default(NULL);
            $table->enum('status', ['Vigente', 'Desusado'])->nullable()->default('Vigente');
            $table->foreign('lvl_id')->references('id')->on('courses_levels');            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
