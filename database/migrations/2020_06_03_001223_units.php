<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Units extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);
            $table->unsignedBigInteger('planing_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('planing_id')->references('id')->on('planings_class');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
