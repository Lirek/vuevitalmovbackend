<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlaningEv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planings_class', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('clases_give')->nullable()->default(NULL);
            $table->json('class_schedule')->nullable()->default(NULL);
            $table->integer('missing_clases')->nullable()->default(NULL);
            $table->integer('lost_class')->nullable()->default(NULL);
            $table->integer('next_class')->nullable()->default(NULL);
            $table->integer('prev_class')->nullable()->default(NULL);
            $table->date('start_date')->nullable()->default(NULL);
            $table->date('end_date')->nullable()->default(NULL);
            $table->unsignedBigInteger('teacher_id')->nullable()->default(NULL);
            $table->integer('number_class_week')->nullable()->default(NULL);            
            $table->foreign('teacher_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
