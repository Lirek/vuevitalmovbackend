<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PhysicalActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity', function (Blueprint $table) {            
            $table->increments('id');
            $table->string('name');
            $table->enum('status', ['Activo', 'Inactivo'])->nullable()->default('Activo');
            $table->float('mets');
            $table->foreign('physical_activity_category_id')->references('id')->on('physical_activity_category');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
