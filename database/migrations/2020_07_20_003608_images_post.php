<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImagesPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_images', function (Blueprint $table) {
            $table->id();
            $table->text('url')->nullable()->default(NULL);            
            $table->unsignedBigInteger('post_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('post_id')->references('id')->on('post');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
