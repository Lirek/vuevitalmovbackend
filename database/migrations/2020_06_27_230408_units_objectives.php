<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UnitsObjectives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objectives_units', function (Blueprint $table) {            
            $table->unsignedBigInteger('unit_id')->nullable()->default(NULL);
            $table->unsignedBigInteger('objective_id')->nullable()->default(NULL);
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('objective_id')->references('id')->on('objectives');
            $table->primary('unit_id','objective_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
