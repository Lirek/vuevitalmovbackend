<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class physical_activity_categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Ciclismo','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Acondicionamiento Físico','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Danza/Baile','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Caza y Pesca','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividades Domésticas','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Reparaciones Domésticas','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Inactividad','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Césped y Jardín','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Misceláneas','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Instrumentos Musicales','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Ocupación u Oficios','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Carrera','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Cuidado Personal','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividad Sexual','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Deportes','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Transporte','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Caminata','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividades Acuáticas','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividades Invernales','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividades Religiosas','Activo',null)");
        DB::insert("INSERT INTO `physical_activity_category`(`id`,`name`,`status`,`description`) VALUES(null,'Actividades Voluntarias','Activo',null)");
    }
}
