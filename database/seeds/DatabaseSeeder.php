<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(FileGroupSeeder::class);
        $this->call(courseLvlsSeeder::class);
        $this->call(regionsSeeder::class);
        $this->call(provincesSeeder::class);
        $this->call(comunesSeeder::class);
        $this->call(ActitudsSeeder::class);
        $this->call(AxisObjectivesSeeder::class);
        $this->call(ObjectivesSeeder::class);
    }
}
