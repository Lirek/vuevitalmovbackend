<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AxisObjectivesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `axis_objectives` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
        (1, 'Habilidades motrices', '2020-06-06 01:16:39', '2020-06-06 01:16:39', NULL),
        (2, 'Vida activa y saludable', '2020-06-06 01:17:09', '2020-06-06 01:17:09', NULL),
        (3, 'Seguridad, juego limpio y liderazgo', '2020-06-06 01:17:41', '2020-06-06 01:17:41', NULL),
        (4, 'Responsabilidad personal y social en la actividad física y el deporte', '2020-06-06 01:18:13', '2020-06-06 01:18:13', NULL);");
    }
}
