<?php

use Illuminate\Database\Seeder;
use App\Components\Core\Models\CoursesLevels;

class courseLvlsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $basic1 = new CoursesLevels;
        $basic1->name = '1º';
        $basic1->save();

        $basic2 = new CoursesLevels;
        $basic2->name = '2º';
        $basic2->save();

        $basic3 = new CoursesLevels;
        $basic3->name = '3°';
        $basic3->save();

        $basic4 = new CoursesLevels;
        $basic4->name = '4°';
        $basic4->save();

        $basic5 = new CoursesLevels;
        $basic5->name = '5°';
        $basic5->save();

        $basic6 = new CoursesLevels;
        $basic6->name = '6°';
        $basic6->save();

        $basic7 = new CoursesLevels;
        $basic7->name = '7°';
        $basic7->save();

        $basic8 = new CoursesLevels;
        $basic8->name = '8º';
        $basic8->save();

        $med1 = new CoursesLevels;
        $med1->name = '1M';
        $med1->save();

        $med2 = new CoursesLevels;
        $med2->name = '2M';
        $med2->save();

        $amed1 = new CoursesLevels;
        $amed1->name = '3M';
        $amed1->save();

        $amed2 = new CoursesLevels;
        $amed2->name = '4M AC';
        $amed2->save();

        $amed3 = new CoursesLevels;
        $amed3->name = '4M BC';
        $amed3->save();
    }
}
