<?php

use App\Components\User\Models\Group;
use App\Components\User\Models\Permission;
use App\Components\User\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create the super user permission
        $permissionSuperUser = Permission::create([
            'title' => 'SuperUsuario',
            'description' => 'Permiso de Superusuario',
            'key' => Permission::SUPER_USER_PERMISSION_KEY,
        ]);

        $permissionProfesor = Permission::create([
            'title' => 'Profesor',
            'description' => 'Permiso de Profesor',
            'key' => Permission::PROFESOR_PERMISSION_KEY,
        ]);

        $permissionEstudent = Permission::create([
            'title' => 'Estudiante',
            'description' => 'Permisos de Estudiantes',
            'key' => Permission::STUDENT_PERMISSION_KEY,
        ]);

        $permissionDirector = Permission::create([
            'title' => 'Directivo',
            'description' => 'Permisos de directivo',
            'key' => Permission::DIRECTIVE_PERMISSION_KEY,
        ]);

        $permissionUser = Permission::create([
            'title' => 'Usuario Libre',
            'description' => 'Permisos de Usuario Libre',
            'key' => Permission::FREE_USER_PERMISSION_KEY,
        ]);

        $permissionTrainer = Permission::create([
            'title' => 'Entrenador',
            'description' => 'Permisos de Entrenador',
            'key' => Permission::TRAINER_PERMISSION_KEY,
        ]);

        // create all other permissions
        $permissionSample1 = Permission::create([
            'title' => 'User Create',
            'description' => 'Permission to create user. This is an example permission only',
            'key' => 'user.create',
        ]);
        $permissionSample2 = Permission::create([
            'title' => 'User Edit',
            'description' => 'Permission to edit user. This is an example permission only',
            'key' => 'user.edit',
        ]);
        $permissionSample3 = Permission::create([
            'title' => 'User Delete',
            'description' => 'Permission to delete user. This is an example permission only',
            'key' => 'user.delete',
        ]);

        // create super user group
        $groupSuperUser = Group::create([
            'name' => Group::SUPER_USER_GROUP_NAME,
            'permissions' => [
                [
                    'title' => 'Super User',
                    'description' => 'Superuser permission',
                    'key' => Permission::SUPER_USER_PERMISSION_KEY,
                    'value' => 1
                ]
            ]
        ]);

        $groupProfesor = Group::create([
            'name' => Group::PROFESOR_GROUP_NAME,
            'permissions' => [
                [
                    'title' => 'Profesor',
                    'description' => 'Permiso de Profesor',
                    'key' => Permission::PROFESOR_PERMISSION_KEY,
                    'value' => 1
                ]
            ]
        ]);

        $groupStudent = Group::create([
            'name' => Group::ESTUDENT_GROUP_NAME,
            'permissions' => [
                [
                    'title' => 'Estudiante',
                    'description' => 'Permiso de Estudiante',
                    'key' => Permission::STUDENT_PERMISSION_KEY,
                    'value' => 1
                ]
            ]
        ]);

        $groupDirective = Group::create([
            'name' => Group::DIRECTIVE_GROUP_NAME,
            'permissions' => [
                [
                    'title' => 'Directivo',
                    'description' => 'Permiso de Directivo',
                    'key' => Permission::DIRECTIVE_PERMISSION_KEY,
                    'value' => 1
                ]
            ]
        ]);

        $groupTrainer = Group::create([
            'name' => Group::TRAINER__GROUP_NAME,
            'permissions' => [
                [
                    'title' => 'Entrenador',
                    'description' => 'Permiso de Entrenador',
                    'key' => Permission::TRAINER_PERMISSION_KEY,
                    'value' => 1
                ]
            ]
        ]);


        $groupSuperUser->addPermission($permissionSuperUser,Permission::PERMISSION_ALLOW);

        // create normal user
        $groupDefaultUser = Group::create([
            'name' => Group::DEFAULT_USER_GROUP_NAME,
            'permissions' => 
            [
                'title' => 'Usuario Libre',
                'description' => 'Permiso de Usuario Libre',
                'key' => Permission::FREE_USER_PERMISSION_KEY,
                'value' => 1
            ]
        ]);

        // create admin account
        $AdminUser = User::create([
            'name' => 'Admin',
            'email' => 'admin@vmove.com',
            'password' => '12345678',
            'remember_token' => Str::random(10),
            'permissions' => [],
            'last_login' => \Carbon\Carbon::now(),
            'active' => \Carbon\Carbon::now(),
            'activation_key' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        ]);

        // make super user
        $AdminUser->groups()->attach($groupSuperUser);
    }
}
