import Vue from 'vue';
import Router from 'vue-router';
import store from '../common/Store';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
        },
        {
            name: 'dashboard',
            path: '/dashboard',
            component: require('./dashboard/Home'),
        },
        {
            path: '/users',
            component: require('./users/Users'),
            children: [
                {
                    path:'/',
                    name:'users.list',
                    component: require('./users/components/UserLists')
                },
                {
                    path:'create',
                    name:'users.create',
                    component: require('./users/components/UserFormAdd')
                },
                {
                    path:'edit/:id',
                    name:'users.edit',
                    component: require('./users/components/UserFormEdit'),
                    props: (route) => ({propUserId: route.params.id}),
                },
                {
                    path:'groups',
                    name:'users.groups.list',
                    component: require('./users/components/GroupLists')
                },
                {
                    path:'groups/create',
                    name:'users.groups.create',
                    component: require('./users/components/GroupFromAdd')
                },
                {
                    path:'groups/edit/:id',
                    name:'users.groups.edit',
                    component: require('./users/components/GroupFromEdit'),
                    props: (route) => ({propGroupId: route.params.id}),
                },
                {
                    path:'permissions',
                    name:'users.permissions.list',
                    component: require('./users/components/PermissionLists')
                },
                {
                    path:'permissions/create',
                    name:'users.permissions.create',
                    component: require('./users/components/PermissionFormAdd')
                },
                {
                    path:'permissions/edit/:id',
                    name:'users.permissions.edit',
                    component: require('./users/components/PermissionFormEdit'),
                    props: (route) => ({propPermissionId: route.params.id}),
                },
            ]
        },
        {
            name: 'files',
            path: '/files',
            component: require('./files/Files'),
        },
        {
            path: '/settings',
            component: require('./settings/Settings'),
            children: [
                {
                    path:'/',
                    name:'plannings.index',
                    component: require('./settings/components/PlaningsIndex')                   
                },
                {
                    path:'create',
                    name:'plannings.create',
                    component: require('./settings/components/PlaningsCreate')                   
                },

            ]
        },
        {
            path: '/indicators',
            component: require('./indicators/Indicators'),
            children: [
                {
                    path:'/',
                    name:'indicators.index',
                    component: require('./indicators/components/IndicatorsIndex')                   
                },
                {
                    path:'create',
                    name:'indicators.create',
                    component: require('./indicators/components/IndicatorsCreate')                   
                },
                {
                    path:'list',
                    name:'indicators.list',
                    component: require('./indicators/components/IndicatorsList')                   
                },
                {
                    path:'Customlist',
                    name:'indicators.Customlist',
                    component: require('./indicators/components/CustomIndicatorsList')                   
                },

            ]
        },
        {
            path: '/systemConfig',
            component: require('./system_configs/SystemConfigs'),
            children: [
                {
                    path:'/',
                    name:'systemConfig.index',
                    component: require('./system_configs/components/SystemConfigsIndex')                   
                },
                {
                    path:'/unitsTemplates',
                    name:'systemConfig.unitsTemplatesList',
                    component: require('./system_configs/components/unitsTemplatesList')                   
                },
                {
                    path:'/objectives',
                    name:'systemConfig.objectives',
                    component: require('./system_configs/components/ObjectivesList')                   
                },
                {
                    path:'/actitudes',
                    name:'systemConfig.actitudes',
                    component: require('./system_configs/components/ActitudesList')                   
                },
                {
                    path:'/itemsConfirmation',
                    name:'systemConfig.ItemsConfirmation',
                    component: require('./system_configs/components/ItemsConfirmation')                   
                }                
            ]
        },
        {
            path: '/social_media',
            component: require('./social_media/SocialMedia'),
            children: [
                {
                    path:'/',
                    name:'SocialMedia.index',
                    component: require('./social_media/components/SocialMediaIndex')                   
                },
            ]
        },  
        {
            path: '/emplyoees',
            component: require('./employee/employee'),
            children: [
                {
                    path:'/',
                    name:'employee.index',
                    component: require('./employee/components/EmployeeIndex')                   
                },
                {
                    path:'/listEmployee',
                    name:'employee.list',
                    component: require('./employee/components/EmployeeList')                   
                },
            ]
        },
        {
            path: '/locations',
            component: require('./locations/locations'),
            children: [
                {
                    path:'/',
                    name:'locations.index',
                    component: require('./locations/components/LocationsIndex')                   
                },
                {
                    path:'/listLocations',
                    name:'locations.list',
                    component: require('./locations/components/LocationsList')                   
                },
                {
                    path:'/listGyms',
                    name:'gyms.list',
                    component: require('./locations/components/GymsList')                   
                },
                {
                    path:'/listStores',
                    name:'stores.list',
                    component: require('./locations/components/StoresList')                   
                },
            ]
        },
        {
            path: '/lesson_plan',
            component: require('./lesson_plan/LessonPlan'),
            children: [
                {
                    path:'/',
                    name:'lesson_plan.index',
                    component: require('./lesson_plan/components/LessonsPlansIndex')                   
                },
                {
                    path:'/list_lesson_plan',
                    name:'lesson_plan.list',
                    component: require('./lesson_plan/components/LessonsPlansList')                   
                },
                {
                    path:'/create_routine',
                    name:'routine.create',
                    component: require('./lesson_plan/components/RoutineCreate')                   
                },
            ]
        },
        {
            path: '/students',
            component: require('./students/students'),
            children: [
                {
                    path:'/',
                    name:'students.index',
                    component: require('./students/components/StudentsIndex')                   
                },
                {
                    path:'/list_courses',
                    name:'courses.list',
                    component: require('./students/components/CoursesList')                   
                },
                {
                    path:'/list_students',
                    name:'students.list',
                    component: require('./students/components/StudentsList')                   
                },
            ]
        },
        {
            path: '/ProfesorsUsersGroups',
            component: require('./profeosr_users_groups/ProfesorsUsersGroups'),
            children: [
                {
                    path:'/',
                    name:'ProfesorsUsersGroups.index',
                    component: require('./profeosr_users_groups/components/ProfesorsUsersGroupsIndex')                   
                },
            ]
        },            
    ],
});

router.beforeEach((to, from, next) => {
    store.commit('showLoader');
    next();
});

router.afterEach((to, from) => {
    setTimeout(()=>{
        store.commit('hideLoader');
    },1000);
});

export default router;