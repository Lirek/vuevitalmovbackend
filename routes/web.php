<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Front\\HomeController@index')->name('front.home');
Route::get('files/{id}/preview','Front\\FileController@filePreview')->name('front.file.preview');
Route::get('files/{id}/download','Front\\FileController@fileDownload')->name('front.file.download');

Auth::routes();

// NOTE:
// remove the demo middleware before you start on a project, this middleware if only
// for demo purpose to prevent viewers to modify data on a live demo site

// admin
Route::prefix('admin')->namespace('Admin')->middleware(['auth'])->group(function()
{
    // single page
    Route::get('/', 'SinglePageController@displaySPA')->name('admin.spa');

    // resource routes
    Route::resource('users','UserController');
    Route::resource('groups','GroupController');
    Route::resource('permissions','PermissionController');
    Route::resource('files','FileController');
    Route::resource('file-groups','FileGroupController');
    Route::resource('objectives','ObjectivesControllers');
    Route::resource('courseLevels','CourseLevelsController');
    Route::resource('indicators','IndicatorsController');
    Route::resource('custom-indicators','CustomIncatorsController');
    Route::resource('units-templates','UnitsTemplateController');
    //custom routes
    Route::get('objectives-course/{id}','ObjectivesControllers@CourseObjective');
    Route::post('priority-objective/{id}','ObjectivesControllers@ObjectivePriority');
    Route::post('objective-status/{id}','ObjectivesControllers@ObjectiveStatus');
    Route::get('objectives-indicators/{id}','IndicatorsController@ObjectiveIndicators');
    Route::get('indicatorsBarCharData/','IndicatorsController@BarCharData');
    Route::get('indicators-count/','IndicatorsController@countIndicator');
    Route::get('custom-indicators-count/','CustomIncatorsController@countCustomIndicator');
    Route::get('actituds-year/{id}','ActitudController@ActitudsLvl');
    Route::get('custom-indicators-admin/','CustomIncatorsController@CustomIndicatorsProbation');
    Route::get('actituds/','ActitudController@index');
    Route::put('actituds/{id}','ActitudController@update');
    Route::post('actitudsCreate/','ActitudController@createActitud');
    Route::post('actituds/{id}','ActitudController@delete');
    Route::get('locationsCount/','LocationsController@countLocations');
    Route::get('units-templates-preview/{lvl_id}','UnitsTemplateController@getCoursePlanningsByCourse');
    
    Route::get('getGyms/','LocationsController@GetGymsLocation');
    Route::post('save_gym/','LocationsController@SaveGymLocation');

    Route::get('getStores/','LocationsController@getStores');
    Route::post('save_store/','LocationsController@SaveStore');

    Route::get('getLocations/','LocationsController@GetLocations');
    Route::post('save_location/','LocationsController@SaveLocations');

    Route::get('getRegions/','RegionController@GetAllRegions');
    Route::get('getProvinces/','RegionController@GetAllProvinces');
    Route::get('getComunnes/','RegionController@GetAllCommines');

    Route::get('countEventsTraning/','EventsTrainingController@countEventsTraining');
    
    Route::get('getLessonPlan/','LessonPlanController@getLessonsPlans');
    Route::get('GetExercises','LessonPlanController@getExercises');

    Route::get('GetProvinceCommune/{id}','RegionController@GetProvinceCommune');    
    Route::get('GetProvinceRegion/{id}','RegionController@GetProvinceRegion');

    Route::get('GetStudents','StudentController@listStudents');
    
    Route::get('GetStudentsCoursesCount','StudentController@getStudentsCoursesCount');  
    Route::get('GetCourses','CourseController@listCourses');  

    
    
    
});