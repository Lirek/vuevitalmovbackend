<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::post('login', 'Mobile\JWTAuthController@authenticate');
    Route::post('createUser','Mobile\MobileUserController@CreateUser');

Route::group(['middleware' => ['jwt.verify']], function() {
    
    Route::get('userData', 'Mobile\JWTAuthController@getAuthenticatedUser');
    Route::get('follows', 'Mobile\MobileUserController@Followers');
    Route::get('followers', 'Mobile\MobileUserController@MyFollowers');
    Route::get('getUser/{id}', 'Mobile\MobileUserController@GetUserRef');
    Route::post('followUser', 'Mobile\MobileUserController@FollowUser');
    Route::post('updatePassword ', 'Mobile\MobileUserController@UpdatePassword');
    Route::post('searchUser','Mobile\MobileSearchController@SearchUsers');
    Route::post('searchLocations','Mobile\MobileSearchController@SearchLocations');
    Route::post('UpdatePersonalData','Mobile\MobileUserController@UpdatePersonalData');
    Route::post('updateAvatar','Mobile\MobileUserController@UpdateAvatar');
    
    //---------Posts Routes----------------------------------------------------

    Route::post('createPost','Mobile\MobilePostControler@CreatePost');
    Route::post('submitComments/{id}','Mobile\MobilePostControler@SubmitPostComments');
    Route::get('likePost/{id}','Mobile\MobilePostControler@LikePub');
    Route::get('getPosts', 'Mobile\MobilePostControler@GetPosts');
    Route::get('getComments/{id}','Mobile\MobilePostControler@getComments');
    Route::get('myPost','Mobile\MobilePostControler@myPosts');
    Route::get('getPost/{id}','Mobile\MobilePostControler@getPost');
    Route::post('UpdatePost/{id}','Mobile\MobilePostControler@UpdatePost');
    Route::post('DeletePost/{id}','Mobile\MobilePostControler@DeletePost');
    
    //-----------Activity Routes------------------------------------------------
    Route::get('getActivity','Mobile\MobileActivityController@getActivity');
    

    
});

