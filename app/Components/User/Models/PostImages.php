<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;
class PostImages extends Model
{
    protected $fillable = ['url', 'post_id'];

    public function geturlAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}


