<?php

namespace App\Components\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Storage;
use URL;
/**
 * Class User
 * @package App\Components\User\Models
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property array $permissions
 * @property string|null $active
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','institute_id', 'password','remember_token','permissions','last_login','active','activation_key','avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * the validation rules
     *
     * @var array
     */
    public static $rules = [];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function groups()
    {
        return $this->belongsToMany('App\Components\User\Models\Group','user_group_pivot_table','user_id');
    }

    public function Followers()
    {
        return $this->hasMany('App\Components\User\Models\UserFollows');
    }

    public function Followed()
    {
        return $this->hasMany('App\Components\User\Models\UserFollows', 'follow_id');
    }

    public function Posts()
    {
        return $this->hasMany('App\Components\User\Models\Post','user_id');
    }

    public function Likes()
    {
        return $this->hasMany('App\Components\User\Models\Likes','user_id');
    }

    public function Comments()
    {
        return $this->hasMany('App\Components\User\Models\PostComemnts','user_id');
    }
    
    public function Institute()
    {
        return $this->belongsTo('App\Components\Core\Models\Institutions', 'institute_id');
    }

 
    //----------------Acesors------------------------------------------------------
    public function getavatarAttribute($value)
    {
        if($value==null)
        {
            return URL::asset(Storage::url('user-placeholder.jpg'));
        }
        else
        {
            return URL::asset(Storage::url($value));
        }
    }
}
