<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;

class PostComemnts extends Model
{
    protected $table = 'post_comments';
    protected $fillable = ['comment','user_id', 'post_id'];


    public function Post()
    {
        return $this->belongsTo('App\Components\User\Models\Post','post_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }
}
