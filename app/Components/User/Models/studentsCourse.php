<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;
;

class studentsCourse extends Model
{
    protected $table = 'course_student';
    protected $fillable = ['course_id','student_id'];
}
