<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserFollows extends Model
{
    
    protected $fillable = ['follow_id','user_id','status'];
    protected $table = 'user_follows';
    public $incrementing = false;
    

}
