<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;

class CustomIndicators extends Model
{
    protected $table = 'custom_indicators';
    protected $fillable = ['name','description','status'];

    public function objectives()
    {
        return $this->belongsTo('App\Components\Core\Models\Objectives', 'objectives_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }
}
