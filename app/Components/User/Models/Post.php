<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['title', 'description','user_id', 'post_category_id','visibility'];

    public function Images()
    {
        return $this->hasMany('App\Components\User\Models\PostImages','post_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }

    public function Likes()
    {
        return $this->hasMany('App\Components\User\Models\Likes','post_id');
    }

    public function Comments()
    {
        return $this->hasMany('App\Components\User\Models\PostComemnts','post_id');
    }


}
