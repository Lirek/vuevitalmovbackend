<?php

namespace App\Components\User\Models;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    protected $table = 'post_likes';
    protected $fillable = ['status','user_id', 'post_id'];

    public function Post()
    {
        return $this->belongsTo('App\Components\User\Models\Post','post_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }
}
