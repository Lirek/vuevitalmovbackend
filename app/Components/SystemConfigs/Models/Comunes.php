<?php

namespace App\Components\SystemConfigs\Models;

use Illuminate\Database\Eloquent\Model;

class Comunes extends Model
{
    protected $table = 'comunas';
    
    protected $fillable = ['name','latitud','longitud','comuna','cut','provincia_id','id'];

    public function Province()
    {
        return $this->belongsTo('App\Components\SystemConfigs\Models\Provinces', 'provincia_id');
    }
}
