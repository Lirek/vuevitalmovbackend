<?php

namespace App\Components\SystemConfigs\Models;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
    protected $table = 'regiones';
    
    protected $fillable = ['region','abreviatura','capital','id'];

    public function Provinces()
    {
        return $this->hasMany('App\Components\SystemConfigs\Models\Provinces', 'region_id');
    }

}
