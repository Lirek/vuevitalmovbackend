<?php

namespace App\Components\SystemConfigs\Models;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected $table = 'provincias';
    
    protected $fillable = ['provincia','region_id'];

    public function Region()
    {
        return $this->belongsTo('App\Components\SystemConfigs\Models\Regions', 'region_id');
    }

    public function Communes()
    {
        return $this->hasMany('App\Components\SystemConfigs\Models\Comunes', 'provincia_id');
    }
}
