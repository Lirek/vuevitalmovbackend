<?php

namespace App\Components\SystemConfigs\Models;

use Illuminate\Database\Eloquent\Model;

class UnitsTemplates extends Model
{
    protected $table = 'units_skeletons';
    protected $fillable = ['name','description','unit_json','lvl_id'];
    
    public function course_levels()
    {
        return $this->belongsTo('App\Components\Core\Models\CoursesLevels', 'lvl_id');
    }

}
