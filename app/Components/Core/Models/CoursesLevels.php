<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class CoursesLevels extends Model
{
    protected $table = 'courses_levels';
    protected $fillable = ['name'];

    public function courses()
    {
        return $this->hasMany('App\Components\Core\Models\Course', 'level_id');
    }

    public function objectives()
    {
        return $this->hasMany('App\Components\Core\Models\Objectives', 'course_id');
    }
}
