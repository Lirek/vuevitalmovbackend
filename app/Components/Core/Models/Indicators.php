<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Indicators extends Model
{
    protected $table = 'indicators';
    protected $fillable = ['name','description'];

    public function objectives()
    {
        return $this->belongsTo('App\Components\Core\Models\Objectives', 'oas_id');
    }
}
