<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class LessonPlan extends Model
{
    protected $table = 'lesson_plan';
    
    protected $fillable = ['title','image','locations_id','description','type_lesson_plan_own','type_lesson_plan','frecuency','ammount','user_id','category_id','level'];

    public function Location()
    {
        return $this->belongsTo('App\Components\Core\Models\Locations', 'locations_id');
    }

    public function Lessons()
    {
        return $this->hasMany('App\Components\Core\Models\Lessons', 'category_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Components\Core\Models\CategoryTraining', 'category_id');
    }

    public function getimageAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}
