<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Axis extends Model
{

    protected $table = 'axis_objectives';
    protected $fillable = ['name'];

    public function objectives()
    {
        return $this->hasMany('App\Components\Core\Models\Objectives', 'axis_id');
    }
}
