<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class Exercise extends Model
{
    protected $table = 'exercise';
    
    protected $fillable = ['description','file','user_id','title'];

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }

    public function Lessons()
    {
        return $this->belongsToMany('App\Components\Core\Models\Locations','lesson_exercise','exersice_id','lesson_id');
    }

    public function getfileAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}
