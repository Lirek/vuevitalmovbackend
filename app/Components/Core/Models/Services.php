<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';
    
    protected $fillable = ['description','file','locations_id'];

    public function Location()
    {
        return $this->belongsTo('App\Components\Core\Models\Locations', 'locations_id');
    }
}
