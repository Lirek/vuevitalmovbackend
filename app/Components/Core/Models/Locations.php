<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class Locations extends Model
{
    protected $table = 'locations';
    
    protected $fillable = ['description','title','avatar','type_owner','type_location','user_id','comuna_id','lat','lon','addres'];

    public function Services()
    {
        return $this->hasMany('App\Components\Core\Models\Services', 'locations_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }

    public function Commune()
    {
        return $this->belongsTo('App\Components\SystemConfigs\Models\Comunes', 'comuna_id');
    }

    public function getavatarAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}
