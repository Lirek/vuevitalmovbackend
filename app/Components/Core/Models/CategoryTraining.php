<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class CategoryTraining extends Model
{
    protected $table = 'categories_training';
    protected $fillable = ['description','title','file','id'];

    public function Trainings()
    {
        return $this->hasMany('App\Components\Core\Models\LessonPlan', 'category_id');
    }

    public function getfileAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}
