<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Institutions extends Model
{
    protected $table = 'institute';
    protected $fillable = ['name','address','phone','email','web_page','logo'];

    public function Users()
    {
        return $this->hasMany('App\Components\User\Models\User', 'institute_id');
    }
}
