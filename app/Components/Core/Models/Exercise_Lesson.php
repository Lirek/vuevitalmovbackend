<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Exercise_Lesson extends Model
{
    protected $table = 'lesson_exercise';
    protected $fillable = ['id','series','reps','lesson_id','exersice_id'];
}
