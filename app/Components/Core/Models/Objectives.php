<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Objectives extends Model
{
    
    protected $table = 'objectives';
    protected $fillable = ['name','description','priority','status'];

    public function axis()
    {
        return $this->belongsTo('App\Components\Core\Models\Axis', 'axis_id');
    }

    public function course_levels()
    {
        return $this->belongsTo('App\Components\Core\Models\CoursesLevels', 'course_id');
    }
}
