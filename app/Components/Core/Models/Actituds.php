<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Actituds extends Model
{
    protected $table = 'actitudes';
    protected $fillable = ['name','description','lvl_id','status'];

    public function course_levels()
    {
        return $this->belongsTo('App\Components\Core\Models\CoursesLevels', 'lvl_id');
    }
}
