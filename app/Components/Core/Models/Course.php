<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['name','plan_id','teacher_id','level_id'];

    public function Students()
    {
        return $this->hasManyThrough('App\Components\User\Models\User',
                                     'App\Components\User\Models\studentsCourse',
                                     'course_id',
                                     'id',
                                     'id',
                                     'student_id'
                                    );
    }

    public function Level()
    {
        return $this->belongsTo('App\Components\Core\Models\CoursesLevels', 'level_id');
    }

    public function Plan()
    {
        return $this->belongsTo('App\Components\Core\Models\LessonPlan', 'level_id');
    }

}
