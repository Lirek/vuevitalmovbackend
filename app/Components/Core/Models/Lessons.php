<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class Lessons extends Model
{
    protected $table = 'lessons';
    
    protected $fillable = ['description','file','type_training','order','user_id','location_id','lesson_plan_id'];

    public function Location()
    {
        return $this->belongsTo('App\Components\Core\Models\Locations', 'locations_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }
    
    public function Exercises()
    {
        return $this->belongsToMany('App\Components\Core\Models\Exercise','lesson_exercise','lesson_id','exersice_id');
    }

    public function getfileAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
}
