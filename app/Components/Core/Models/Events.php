<?php

namespace App\Components\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use URL;

class Events extends Model
{
    protected $table = 'events';
    
    protected $fillable = ['title','image','locations_id','description','type_events_own','type_events','frecuency','duration','user_id'];

    public function Location()
    {
        return $this->belongsTo('App\Components\Core\Models\Locations', 'locations_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Components\User\Models\User', 'user_id');
    }

    public function getimageAttribute($value)
    {
        return URL::asset(Storage::url($value));
    }
    
}
