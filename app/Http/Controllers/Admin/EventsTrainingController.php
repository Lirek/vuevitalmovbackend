<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Components\Core\Models\Locations;
use App\Components\Core\Models\LessonPlan;
use App\Components\Core\Models\Events;
use App\Components\User\Models\User;

class EventsTrainingController extends Controller
{
    public function countEventsTraining()
    {
        $trainigs = LessonPlan::all();
        $events = Events::all();
        
        $collection = new Collection;
        $collection->push(['Trainings'=>$trainigs->count(),'Events'=> $events->count()]);
        return $this->sendResponseOk($collection,"Locacions");
    }
}
