<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\Core\Models\Actituds;
use Illuminate\Support\Facades\Log;

class ActitudController extends AdminController
{
    public function index()
    {
        $Actituds=Actituds::with('course_levels')->get();
        return $this->sendResponseOk($Actituds,"OK");
    }

    public function ActitudsLvl($id)
    {
        $Actituds=Actituds::where('lvl_id',$id)->get();
        return $this->sendResponseOk($Actituds,"OK");
    }

    public function createActitud(Request $request)
    {
        $validate = validator($request->all(),[
            'course_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'status'=> 'required',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        Log::alert($request->all());
        $Actituds = new Actituds;
        $Actituds->name = $request->name;
        $Actituds->description = $request->description;
        $Actituds->lvl_id = $request->course_id;
        $Actituds->status = $request->status;
        $Actituds->save();
            
        $this->sendResponseCreated($Actituds);        
    }

    public function update(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'course_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'status'=> 'required',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $Actituds = Actituds::find($id);
        $Actituds->name = $request->name;
        $Actituds->description = $request->description;
        $Actituds->lvl_id = $request->course_id;
        $Actituds->status = $request->status;
        $Actituds->save();
        
        $this->sendResponseUpdated($Actituds);        

    }

    public function delete(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'status' => 'required'
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
       
        $Actituds = Actituds::find($id);
        $Actituds->status = $request->status;
        $Actituds->save();
                
        $this->sendResponseUpdated($Actituds);    
    }
}
