<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Auth;

use App\Components\User\Models\User;
use App\Components\User\Models\Group;
use App\Components\Core\Models\Course;
use App\Components\User\Models\studentsCourse;

class CourseController extends Controller
{
    public function CreateCourse()
    {
        # code...
    }

    public function listCourses()
    {
        $user = Auth::user();
        $coursesList = new Collection;
        
        if ($user->inGroup('Directivos')) 
        {
            $teachers = User::where('institute_id',$user->institute_id)->get();
            
            foreach($teachers as $teacher)
            {      
                if($teacher->inGroup('Profesores'))
                {          
                    $Courses = Course::where('teacher_id',$teacher->id)->get();
                    foreach ($Courses as $Course) 
                    {
                        $coursesList->push([
                                        "id"=> $Course->id, 
                                        "Nombre"=> $Course->name, 
                                        "Profesor" => $teacher->name, 
                                        "Curso"=> $Course->Level()->first()->name, 
                                        "Estudiantes" => $Course->Students()->count()
                                    ]);
                    }
                }                                    
            }                        
        }

        if ($user->inGroup('Profesores')) 
        {
            $Courses = Course::where('teacher_id',$user->id)->get();
            
            foreach($Courses as $Course)
            {
                $coursesList->push([
                    "id"=> $Course->id, 
                    "Nombre"=> $Course->name,  
                    "Curso"=> $Course->Level()->first()->name, 
                    "Estudiantes" => $Course->Students()->count()
              ]);
            }
        }

        return $this->sendResponseOk($coursesList,"list courses.");
    }
}
