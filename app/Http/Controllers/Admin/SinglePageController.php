<?php
/**
 * Created by PhpStorm.
 * User: darryldecode
 * Date: 4/2/2018
 * Time: 8:40 AM
 */

namespace App\Http\Controllers\Admin;


use App\Components\Core\Menu\MenuItem;
use App\Components\Core\Menu\MenuManager;
use App\Components\User\Models\User;

class SinglePageController extends AdminController
{
    public function displaySPA()
    {
        /**
         * @var User $currentUser
         */
        $currentUser = \Auth::user();
        $menuManager = new MenuManager();
        $menuManager->setUser($currentUser);
        $menuManager->addMenus([
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser','directive'],
                'label'=>'Panel Principal',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'dashboard',
                'route_type'=>'vue',
                'route_name'=>'dashboard',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['profesor'],
                'label'=>'Equipos',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'group',
                'route_type'=>'vue',
                'route_name'=>'ProfesorsUsersGroups.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser'],
                'label'=>'Usuarios',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'person',
                'route_type'=>'vue',
                'route_name'=>'users.list',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser'],
                'label'=>'Archivos',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'cloud_circle',
                'route_type'=>'vue',
                'route_name'=>'files',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser'],
                'label'=>'Locaciones',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-map',
                'route_type'=>'vue',
                'route_name'=>'locations.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser'],
                'label'=>'Entrenamientos',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-hiking',
                'route_type'=>'vue',
                'route_name'=>'lesson_plan.index',
                'visible'=>true,
            ]),            
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser','profesor','directive'],
                'label'=>'Planificaciones',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-calendar',
                'route_type'=>'vue',
                'route_name'=>'plannings.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser','profesor','directive'],
                'label'=>'Indicadores',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-chart-line-variant', 
                'route_type'=>'vue',
                'route_name'=>'indicators.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser'],
                'label'=>'Configuraciones',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-settings-outline', 
                'route_type'=>'vue',
                'route_name'=>'systemConfig.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['superuser','profesor','directive'],
                'label'=>'Sociales',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-account-network', 
                'route_type'=>'vue',
                'route_name'=>'SocialMedia.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['directive'],
                'label'=>'Empleados',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-account-tie',
                'route_type'=>'vue',
                'route_name'=>'employee.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['directive','profesor'],
                'label'=>'Estudiantes',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-face',
                'route_type'=>'vue',
                'route_name'=>'students.index',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['directive','profesor'],
                'label'=>'Reportes',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-face',
                'route_type'=>'vue',
                'route_name'=>'dashboard',
                'visible'=>true,
            ]),
            new MenuItem([
                'group_requirements' => [],
                'permission_requirements' => ['institucion','directive'],
                'label'=>'Configuraciones',
                'nav_type' => MenuItem::$NAV_TYPE_NAV,
                'icon'=>'mdi-settings-outline',
                'route_type'=>'vue',
                'route_name'=>'users.list',
                'visible'=>true,
            ]),            
            new MenuItem([
                'nav_type' => MenuItem::$NAV_TYPE_DIVIDER
            ])
        ]);

        $menus = $menuManager->getFiltered();

        view()->share('nav',$menus);

        return view('layouts.admin');
    }
}