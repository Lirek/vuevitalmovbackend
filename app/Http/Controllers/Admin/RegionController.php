<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Components\SystemConfigs\Models\Regions;
use App\Components\SystemConfigs\Models\Provinces;
use App\Components\SystemConfigs\Models\Comunes;

class RegionController extends Controller
{
    public function GetAllRegions()
    {
        $Regions = Regions::all();
        
        return $this->sendResponseOk($Regions,"Regions");
    }
    
    public function GetAllProvinces()
    {
        $Provinces = Provinces::all();
        return $this->sendResponseOk($Provinces,"Regions");
    }
    
    public function GetAllCommines()
    {
        $Comunes = Comunes::all();
        return $this->sendResponseOk($Comunes,"Comunes");
    }
    
    public function GetProvinceRegion($id)
    {
        $Provinces = Provinces::where('region_id','=',$id)->get();
        return $this->sendResponseOk($Provinces,"Provinces");
    }
    
    public function GetProvinceCommune($id)
    {
        $Comunes = Comunes::where('provincia_id','=',$id)->get();        
        return $this->sendResponseOk($Comunes,"Comunes");
    }

}
