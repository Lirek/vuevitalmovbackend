<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\User\Models\User;
use App\Components\Core\Models\Institutions;
use Illuminate\Support\Collection;

class EmployessController extends AdminController
{
    public function ShowEmployees($id)
    {
        $Institution = Institutions::find($id);
        $Employees = new Collection;
        $users = User::where('institute_id',$Institution->id)->get();

        foreach($users as $user)
        {
            if($user->group()->id == 3)
            {
                $name = $user->name;
                $rut = $user->teacher()->rut;
                $type = $user->teacher()->type;
                $Employees->push(['name'=>$name,'rut'=>$rut,'type'=>$type]);
            }
        }
        
        return response()->json($Employees, 200);
    }
}
