<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\SystemConfigs\Models\UnitsTemplates;
use Illuminate\Support\Facades\Log;

class UnitsTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $UnitsTemplates = UnitsTemplates::with('course_levels')->get();
        return $this->sendResponseOk($UnitsTemplates,"Templates OK");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'name' => 'required',
            'description' => 'required',
            'unit_json' => 'required|json',
            'course_id' => 'required|integer',
        ]);

        
        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        $UnitsTemplates =  new UnitsTemplates;
        $UnitsTemplates->name = $request->name;
        $UnitsTemplates->description = $request->description;
        $UnitsTemplates->unit_json = $request->unit_json; 
        $UnitsTemplates->lvl_id = $request->course_id;
        $UnitsTemplates->save();
        return $this->sendResponseCreated($UnitsTemplates);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'name' => 'required',
            'description' => 'required',
            'unit_json' => 'required|json',
            'course_id' => 'required|integer',
        ]);

        
        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $UnitsTemplates = UnitsTemplates::find($id);
        $UnitsTemplates->name = $request->name;
        $UnitsTemplates->description = $request->description;
        $UnitsTemplates->unit_json = $request->unit_json; 
        $UnitsTemplates->lvl_id = $request->course_id;
        $UnitsTemplates->save();
        return $this->sendResponseUpdated($UnitsTemplates);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $UnitsTemplates = UnitsTemplates::find($id);
        $UnitsTemplates->delete();
        return $this->sendResponseDeleted();
      
    }

    public function getCoursePlanningsByCourse($lvl_id)
    {   
        $UnitsTemplates = UnitsTemplates::where('lvl_id','=',$lvl_id)->get();
        
        $UnitsTemplates->map(function ($UnitsTemplate){
            $x =  json_encode($UnitsTemplate->unit_json, JSON_UNESCAPED_UNICODE);
            
            return $UnitsTemplate->unit_json = stripslashes(substr($x,1,-1));
        }); 
        
        return $this->sendResponseOk($UnitsTemplates,"Templates OK");
      
    }
}
