<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\Core\Models\Indicators;
use App\Components\Core\Models\Objectives;
use App\Components\Core\Models\CoursesLevels;


class IndicatorsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicators = Indicators::all();
        $indicators->map(function ($indicator) {
            $indicator->course=$indicator->objectives->course_levels->name;
            $indicator->oas_id=$indicator->objectives->name; 
            return $indicator;
            });
        return $this->sendResponseOk($indicators,"Indicators");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ObjectiveIndicators($id)
    {
        $indicators = Indicators::where('oas_id',$id)->get();
        $indicators->map(function ($indicator) {
                                                $indicator->objectives->name; 
                                                return $indicator;
                                                });
        
        return $this->sendResponseOk($indicators,"Indicators");
    }

    public function BarCharData()
    {
        $indicators = Indicators::all();
        $series;
        $categories=CoursesLevels::with('objectives')->get();
        $i=0;
        $data = collect();
        foreach($categories as $categorie)
        {
            $series=0;
            foreach($categorie->objectives as $objective)
            {                
                $series += $indicators->where('oas_id',$objective->id)->count();
            }
            $data->push(["categorie"=> $categorie->name, "series"=> $series]);
        }
        
        return $this->sendResponseOk($data,"barchar");
        
    }

    public function countIndicator()
    {
        $indicators = Indicators::all()->count();
        return $this->sendResponseOk($indicators,"Indicators");

    }
}
