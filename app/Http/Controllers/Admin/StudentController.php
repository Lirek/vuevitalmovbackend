<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Components\User\Models\User;
use App\Components\User\Models\Group;
use App\Components\Core\Models\Course;
use App\Components\User\Models\studentsCourse;

use Auth;

class StudentController extends Controller
{
    public function listStudents()
    {
        $user = Auth::user();
        $students = User::where('institute_id',$user->institute_id)->get();
        $studentsList = new Collection;

        foreach ($students as $student) 
        {
            if ($student->inGroup('Estudiantes')) 
            {                                                                
            
                $Levels=studentsCourse::where('student_id',$student->id)->first();
                if ($Levels!=null) 
                {
                    $Course = Course::find($Levels->course_id);
                    $student->course_level = $Course->Level()->first()->name;
                    $student->course = $Course->name;
                    $student->course_id = $Course->id;                    
                }
                else
                {
                    $student->course_level = 'Sin Asignar';
                    $student->course = 'Sin Asignar';
                    $student->course_id = null;                    
                }

                
                $studentsList->push($student);
            }
        }
        return $this->sendResponseOk($studentsList,"list students.");
    }

    public function getStudentsCoursesCount()
    {
        $user = Auth::user();
        $studentsCount = 0;
        $coursesCount = 0;

        if ($user->inGroup('Profesores'))
        {
            $Courses = Course::where('teacher_id',$user->id)->get();
            foreach($Courses as $Course)
            {
                $studentsCount+= $Course->Students()->count();
            }

            $coursesCount = Course::where('teacher_id',$user->id)->count();

        }

        if ($user->inGroup('Directivos'))
        {
            $Users = User::where('institute_id',$user->institute_id)->get();
            foreach($Users as $User)
            {
                if($User->inGroup('Estudiantes'))
                {
                    $studentsCount++;
                }
                if($User->inGroup('Profesores'))
                {
                    $coursesCount+=Course::where('teacher_id',$User->id)->count();
                }
            }
        }

        $count = new Collection;
        $count->push(["students"=>$studentsCount, "courses"=>$coursesCount]);
        return $this->sendResponseOk($count,"Count of Students and Courses.");

    }

}
