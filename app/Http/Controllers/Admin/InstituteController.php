<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Components\Core\Models\Institutions;

class InstituteController extends Controller
{
    public function viewInstitute()
    {
        $institutions = Institutions::all();
        return $this->sendResponseOk($institutions,"institutions");
    }

    public function getInstitute($id)
    {
        $institute = Institutions::find($id);
        return $this->sendResponseOk($institute,"institute");
    }

    public function saveInstitute(Request $request)
    {
        $institute = new Institutions;
        $institute->name = $request->name;
        $institute->address = $request->address;
        $institute->phone = $request->phone;
        $institute->email = $request->email;
        $institute->web_page = $request->web_page;
        $institute->logo = Storage::disk('public')->putFile('Institutions/'.$request->name.'/',$request->logo);
        $institute->save();        
        return $this->sendResponseOk($institute,"institute");
    }

    public function updateInstitute(Request $request,$id)
    {
        $institute = Institutions::find($id);
        $institute->name = $request->name;
        $institute->address = $request->address;
        $institute->phone = $request->phone;
        $institute->email = $request->email;
        $institute->web_page = $request->web_page;
        $institute->logo = Storage::disk('public')->putFile('Institutions/'.$request->name.'/',$request->logo);
        $institute->save();        
        return $this->sendResponseOk($institute,"institute");
    }
}
