<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Components\Core\Models\Locations;
use App\Components\Core\Models\LessonPlan;
use App\Components\Core\Models\Lessons;
use App\Components\Core\Models\Exercise;
use App\Components\User\Models\User;

use Auth;

class LessonPlanController extends Controller
{
    public function getLessonsPlans()
    {        
        $LessonPlan = LessonPlan::all();
        return $this->sendResponseOk($LessonPlan,"OK");
    }

    public function saveLessonPlan(Request $request)
    {        
        $LessonPlan = new LessonPlan;
        $LessonPlan;
        $LessonPlan;
        $LessonPlan;
        $LessonPlan;
    }

    public function getExercises()
    {
        $Exercise = Exercise::all();
        return $this->sendResponseOk($Exercise,"OK");
    }

}
