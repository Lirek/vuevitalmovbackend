<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\Core\Models\Objectives;
use Illuminate\Support\Facades\Log;

class ObjectivesControllers extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Objectives::with('course_levels')->get();
        return $this->sendResponseOk($data,"list Objectives ok.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'priority' => 'required',
            'name' => 'required',
            'description' => 'required',
            'status'=> 'required',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        
        $Objectives = Objectives::find($id);
        $Objectives->name = $request->name;
        $Objectives->description = $request->description;
        $Objectives->priority = $request->priority;
        $Objectives->status = $request->status;
        $Objectives->save();

        $this->sendResponseUpdated($Objectives);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CourseObjective($id)
    {
        $data = Objectives::where('course_id',$id)->get();
        return $this->sendResponseOk($data,"OK.");
    }

    public function ObjectivePriority(Request $request,$id)
    {
        $validate = validator($request->all(),[
            'priority' => 'required'
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        
        $Objectives = Objectives::find($id);
        $Objectives->priority = $request->priority;
        $Objectives->save();
        //Log::info($Objectives);
        $this->sendResponseUpdated($Objectives);
    }

    public function ObjectiveStatus(Request $request,$id)
    {
        $validate = validator($request->all(),[
            'status' => 'required'
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        
        $Objectives = Objectives::find($id);
        $Objectives->status = $request->status;
        $Objectives->save();
        Log::info($Objectives);
        $this->sendResponseUpdated($Objectives);
    }
}
