<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\User\Models\CustomIndicators;
use Illuminate\Support\Facades\Log;
use Auth;

class CustomIncatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customIndicators = CustomIndicators::where('user_id',Auth::user()->id)->get();
        $customIndicators->map(function ($customIndicator) {
            $customIndicator->course=$customIndicator->objectives->course_levels->name;
            $customIndicator->oas_id=$customIndicator->objectives->name; 
            return $customIndicator;
            });
        return $this->sendResponseOk($customIndicators,"Indicators");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'name' => 'required',
            'description' => 'required',
            'objective' => 'required',
        ]);
        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());

        $CustomIndicators = new CustomIndicators;
        $CustomIndicators->name = $request->name;
        $CustomIndicators->description = $request->description;
        $CustomIndicators->objectives_id = $request->objective;
        $CustomIndicators->user_id = Auth::user()->id;
        $CustomIndicators->save();

        return $this->sendResponseCreated($CustomIndicators);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = validator($request->all(),[
            'name' => 'required',
            'description' => 'required',
            'objective' => 'required',
        ]);
        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        
        $CustomIndicators = CustomIndicators::find($id);
        $CustomIndicators->name = $request->name;
        $CustomIndicators->description = $request->description;
        $CustomIndicators->objectives_id = $request->objective;
        $CustomIndicators->status = 'En Proceso';
        $CustomIndicators->save();

        return $this->sendResponseUpdated($CustomIndicators);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CustomIndicators = CustomIndicators::find($id);

        if($CustomIndicators->user_id != auth()->user()->id)
        {
            return $this->sendResponseForbidden();
        } 
       
        $CustomIndicators->delete();
        return $this->sendResponseDeleted();

    }

    public function countCustomIndicator()
    {
        $indicators = CustomIndicators::where('user_id',Auth::user()->id)->count();
        return $this->sendResponseOk($indicators,"Indicators");

    }    

    public function CustomIndicatorsProbation()
    {
        $customIndicators = CustomIndicators::where('status','=','En Proceso')->get();
        $customIndicators->map(function ($customIndicator) {
            $customIndicator->course=$customIndicator->objectives->course_levels->name;
            $customIndicator->oas_id=$customIndicator->objectives->name;
            $customIndicator->user_id=$customIndicator->user->name; 
            return $customIndicator;
            });
        return $this->sendResponseOk($customIndicators,"Indicators");
    }
}
