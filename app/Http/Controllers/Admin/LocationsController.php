<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Components\Core\Models\Locations;
use App\Components\Core\Models\Services;
use App\Components\User\Models\User;

use Auth;

class LocationsController extends Controller
{
    public function GetGymsLocation()
    {
        $gyms = Locations::where('type_location','=','Gymnasio')->get();
        $gyms->map(function($gyms){
            $gyms->comunas_id = $gyms->Commune->comuna;
            return $gyms;  
        });
        return $this->sendResponseOk($gyms,"Gyms");
    }

    public function SaveGymLocation(Request $request)
    {
        
        $Gym = new Locations;
        $Gym->description = $request->description;
        $Gym->title = $request->name;
        $Gym->avatar = Storage::disk('public')->putFile('Locations/Gyms',$request->avatar);
        $Gym->type_owner = $request->type_owner;
        $Gym->type_location = 'Gymnasio';
        $Gym->comuna_id = $request->comuna;
        $Gym->addres = $request->address;
        $Gym->save();

        foreach($request->services as $x)
        {
            $service = new Services;
            $service->description = $x;
            $service->locations_id = $Gym->id; 
            $service->save();
        }
        return $this->sendResponseOk($Gym,"OK");
    }

    public function GetLocations()
    {
        $Locacion = Locations::where('type_location','=','Locacion')->get();
        $Locacion->map(function($Locacion){
            $Locacion->comunas_id = $Locacion->Commune->comuna;
            return $Locacion;  
        });
        return $this->sendResponseOk($Locacion,"Locacion");
    }

    public function SaveLocations(Request $request)
    {
        
        $Locacion = new Locations;
        $Locacion->description = $request->description;
        $Locacion->title = $request->name;
        $Locacion->avatar = Storage::disk('public')->putFile('Locations/Places',$request->avatar);
        $Locacion->type_owner = $request->type_owner;
        $Locacion->type_location = 'Locacion';
        $Locacion->comuna_id = $request->comuna;
        $Locacion->addres = $request->address;
        $Locacion->save();

        foreach($request->services as $x)
        {
            $service = new Services;
            $service->description = $x;
            $service->locations_id = $Locacion->id; 
            $service->save();
        }
        return $this->sendResponseOk($Locacion,"OK");

    }

    public function getStores()
    {
        $Tienda = Locations::where('type_location','=','Tienda')->get();
        $Tienda->map(function($Tienda){
            $Tienda->comunas_id = $Tienda->Commune->comuna;
            return $Tienda;  
        });
        return $this->sendResponseOk($Tienda,"Locacion");
    }

    public function SaveStore(Request $request)
    {
        
        $Store = new Locations;
        $Store->description = $request->description;
        $Store->title = $request->name;
        $Store->avatar = Storage::disk('public')->putFile('Locations/Gyms',$request->avatar);
        $Store->type_owner = 'Privada';
        $Store->type_location = 'Tienda';
        $Store->comuna_id = $request->comuna;
        $Store->addres = $request->address;
        $Store->save();

        foreach($request->services as $x)
        {
            $service = new Services;
            $service->description = $x;
            $service->locations_id = $Store->id; 
            $service->save();
        }
        return $this->sendResponseOk($Store,"OK");

    }

    public function countLocations()
    {
        $Tienda = Locations::where('type_location','=','Tienda')->get();
        $Locacion = Locations::where('type_location','=','Locacion')->get();
        $gyms = Locations::where('type_location','=','Gymnasio')->get();
        $collection = new Collection;
        $collection->push(['Stores'=>$Tienda->count(),'Gyms'=> $gyms->count(),'Locations'=> $Locacion->count()]);
        return $this->sendResponseOk($collection,"Locacions");
    }
}
