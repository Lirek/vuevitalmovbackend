<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Components\User\Models\User;
use App\Components\User\Models\UserFollows;
use App\Components\User\Models\Post;
use App\Components\User\Models\Likes;
use App\Components\User\Models\PostComemnts;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;

use URL;
use JWTAuth;
use DB;

class MobileActivityController extends Controller
{
    public function getActivity()
    {
        $user = JWTAuth::user();
        $activity = new Collection;
        
        $likes = Likes::where('user_id','=',$user->id)->with('Post')->get();
        $Posts = Post::where('user_id','=',$user->id)->get();
        $commets = PostComemnts::where('user_id','=',$user->id)->with('Post')->get();
        $followers = UserFollows::where('user_id','=',$user->id)->get();
        $follows = UserFollows::where('follow_id','=',$user->id)->get();
        
        
        if($likes != null)
        {
            foreach($likes as $like)
            {
                        
              if(str_word_count($like->Post->description)<6) 
              {
                  
                  if($like->Post->user_id == $user->id)
                  {
                    $activity->push(['description'=>'Te ha Gustado '.$like->Post->description,'title'=>'Me Gusta','time'=>$like->Post->created_at,'img'=>$like->Post->Images,'avatar'=>$user->avatar]);                    
                  }
              }
              else                
                {
                    $text = str_replace("  ", " ", $like->Post->description);
                    $string = explode(" ", $like->Post->description);
                    $counter=0;
                    $tr = $string[0].' '.$string[1].' '.$string[3].' '.$string[4].' '.$string[5].'...';
                    if($like->Post->user_id == $user->id)
                    {
                        $activity->push(['description'=>'Te ha Gustado '.$tr,'title'=>'Me Gusta','time'=>$like->Post->created_at,'img'=>$like->Post->Images,'avatar'=>$user->avatar]);                                  
                
                    }
                }    
            }
        }
        if($followers != null)
        {
            foreach($followers as $follower)
            {
                $userRef = User::find($follower->follow_id);
                $activity->push(['description'=>'Estas Siguiendo a '.$userRef->name,'title'=>'Seguiendo','time'=>$follower->created_at,'img'=>$userRef->avatar]);
            }
        }
        
        if($follows != null)
        {
            foreach($follows as $follow)
            {
                $userRef = User::find($follow->user_id);
                $activity->push(['description'=>'Te Esta Siguiendo '.$userRef->name,'title'=>'Seguiendote','time'=>$follow->created_at,'img'=>$userRef->avatar]);
            }
        }
        
        if($Posts != null)
        {
            foreach($Posts as $Post)
            {
                $CommentsRefs = PostComemnts::where('post_id','=',$Post->id)->get();
                if($CommentsRefs != null)
                {
                    foreach($CommentsRefs as $CommentsRef)
                    {
                        $userRef = User::find($CommentsRef->user_id);
                        if($userRef->id == $user->id)
                        {
                            $activity->push(['description'=>'Has Comentado '.$Post->description,'title'=>'Comentario','time'=>$Post->created_at,'avatar'=>$user->avatar]);    
                        }
                        else
                        {
                            $activity->push(['description'=>$userRef->name.' Ha Comentado '.$Post->description,'title'=>'Comentario','time'=>$Post->created_at,'avatar'=>$userRef->avatar]);
                        }
                    }
                    
                }
            }
        }
        
        

        return $this->sendResponseOk($activity,"OK");
        
    }
}
