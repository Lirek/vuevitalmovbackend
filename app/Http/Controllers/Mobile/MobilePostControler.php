<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Components\User\Models\User;
use App\Components\User\Models\UserFollows;
use App\Components\User\Models\Post;
use App\Components\User\Models\PostImages;
use App\Components\User\Models\Likes;
use App\Components\User\Models\PostComemnts;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;

use URL;
use JWTAuth;
use DB;


class MobilePostControler extends Controller
{
    public function CreatePost(Request $request)
    {
        try 
        {
           Log::alert($request);
           
           $user = JWTAuth::user();
           $post = new Post;
           $post->title = NULL;
           $post->description = $request->description;
           $post->user_id = $user->id;
           $post->save();
          
                $files = $request->postImage;
                foreach($files as $file)
                {
                    
                    $path = Storage::disk('public')->putFile('post',$file);
                    $image = new PostImages;
                    $image->url = $path;
                    $image->post_id = $post->id;
                    $image->save();
                    
                }
           return $this->sendResponseOk($post->with('Images'),"OK");
        }
        catch (\Exception $e)
        {
             Log::alert($e);
             $post->delete();
             return $e;
        }
                
    }

    public function GetPosts($numbPage)
    {
        $user = JWTAuth::user();
        $id = $user->id;
        $follows = UserFollows::where('follow_id','=',$user->id)->get();        
        $posts = $user->Posts()->with('Images')->get();
        $posts->map(function ($item) 
        {
            $item->likes = $item->Likes()->where('status','=','Like')->count();
            if($item->Likes()->where('status','=','Like')->where('user_id','=',$item->User->id)->first()!=null)
            {
                $item->UserLikes = true;
            }
            else
            {
                $item->UserLikes = false;
            }
             
            $item->comments = $item->Comments()->count();
            $item->user = $item->User;
            return $item;
        });

        foreach ($follows as $key) 
        {   
            $temp = User::find($key->user_id);
            if($temp->Posts()->count()!=0)
            {
                $tempPost = $temp->Posts()->with('Images')->get();
                $tempPost->map(function ($item) 
                {
                    if ($item->Likes()->where('status','=','Like')->where('user_id','=',JWTAuth::user()->id)->first()!=null) 
                    {
                        $item->UserLikes = true;                        
                    }
                    else
                    {
                        $item->UserLikes = false;                        
                    }
                    $item->likes = $item->Likes()->where('status','=','Like')->count();
                    $item->comments = $item->Comments()->count();
                    $item->user = $item->User;
                    return $item;
                });
                foreach($tempPost as $x)
                {
                    $posts->push($x);
                }
            }
        }
        
        $posts->sortBy('created_at');
        $return = $posts->forPage($numbPage,20);
        $returnDif = new Collection;
        if($numbPage != 1)
        {
            foreach($return as $things)
            {
                $returnDif->push($things);
            }
            return $this->sendResponseOk($returnDif,"OK");
        }
        else
        {
            return $this->sendResponseOk($return,"OK");
        }

    }

    public function LikePub($id)
    {
       $user = JWTAuth::user();
       $post = Post::find($id);
       $check = Likes::where('user_id','=',$user->id)->where('post_id','=',$id)->first();
       if ($check != null) 
       {
            if ($check->status == 'Like') 
            {
                $check->status = 'Disliske';
                $check->save();
            }
            else
            {
                $check->status = 'Like';
                $check->save();
            }
        return $this->sendResponseOk($check,"OK");        
       }
       else
       {
        $like = new Likes;
        $like->status = 'Like';
        $like->user_id = $user->id; 
        $like->post_id = $post->id;
        $like->save();            
       }
       
       return $this->sendResponseOk($like,"OK");        
    }

    public function getComments($id)
    {
       $post = Post::find($id);
       $comments = $post->Comments()->get();
       if ($comments != null) 
       {
            $comments->map(function ($item){
                $item->avatar = $item->User->avatar;
                $item->name = $item->User->name;
            });

            return $this->sendResponseOk($comments,"OK");
       }
       else
       {
            $default = new Collection;
            $default->avatar= null;
            $default->name= 'VitalMove';
            $default->user_id = 0;
            $default->post_id = $id;
            return $this->sendResponseOk($default,"OK");
       }
       
        
    }

    public function SubmitPostComments($id, Request $request)
    {
        $comments = new PostComemnts;
        $comments->comment = $request->comment;
        $comments->user_id = JWTAuth::user()->id;
        $comments->post_id = $id;
        $comments->save();
        return $this->sendResponseOk($comments,"OK");
    }
    
    public function myPosts()
    {
        $user = JWTAuth::user();        
        $posts = $user->Posts()->with('Images')->get();
        if ($posts == null) 
        {
            return $this->sendResponseNoContent();
        }
        else
        {
            return $this->sendResponseOk($posts,"OK");
        }
    }
    
    public function getPost($id)
    {
        $post = Post::with('Images')->find($id);
        
        if ($post == null) 
        {
            return $this->sendResponseNoContent();
        }
        else
        {
            return $this->sendResponseOk($post,"OK");
        }
    }
    
    public function UpdatePost($id,Request $request )
    {
        $user = JWTAuth::user();
        $post = Post::find($id);
        
        if($user->id == $post->user_id)
        {
            $post->description = $request->description;
            $post->save();
            return $this->sendResponseOk($post,"OK");
        }
        else
        {
            return $this->sendResponseForbidden();
        }
        
    }
    
    public function DeletePost($id)
    {
        $user = JWTAuth::user();
        $post = Post::find($id);
        
        if($user->id == $post->user_id)
        {
            $images = PostImages::where('post_id','=',$post->id)->delete();
            $likes = Likes::where('post_id','=',$post->id)->delete();
            $comments = PostComemnts::where('post_id','=',$post->id)->delete();
            $post->delete();
            return $this->sendResponseDeleted();
        }
        else
        {
            return $this->sendResponseForbidden();
        }
    }
}
