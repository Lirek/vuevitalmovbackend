<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;

use App\Components\User\Models\User;
use App\Components\User\Models\UserFollows;
use App\Components\Core\Models\Locations;
use App\Components\Core\Models\Services;
use App\Components\Core\Models\Events;

use URL;
use JWTAuth;
use DB;

class MobileSearchController extends Controller
{
    public function SearchUsers(Request $request)
    {
        $data = $request->data;

        $searchResult = User::where('name', 'like', "%{$data}%")->get();
        
        return $this->sendResponseOk($searchResult,"OK");
    }

    public function SearchLocations(Request $request)
    {
        $data = $request->data;

        $searchResult = Locations::where('name', 'like', "%{$data}%")->get();
        
        return $this->sendResponseOk($searchResult,"OK");
    }
}
