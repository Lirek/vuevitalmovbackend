<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Components\User\Models\User;
use Illuminate\Support\Collection;
use App\Components\User\Models\UserFollows;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use URL;
use JWTAuth;
use DB;

class MobileUserController extends Controller
{
    public function UpdatePersonalData(Request $request)
    {
        $validate = validator($request->all(),[
            'email' => 'required|string|email|max:255',
            'name' => 'required|string|max:255',            
        ]);
        
        $user = JWTAuth::user();
        
        if($request->avatar != null)
        {
            $path = Storage::disk('public')->putFile('avatars',$request->file('avatar'));
            $user->avatar = $path;
        }

        if($request->password != null) 
        {
            $user->password = $request->password; 
        }

        if($request->name != null)
        {
            $user->name = $request->name;
        }
        
        $user->email = $request->email;
        
        $user->save();
        $user->avatar = asset(Storage::url($user->avatar));
        return $this->sendResponseOk($user,"OK");        
        
    }

    public function FollowUser(Request $request)
    {
        $user = JWTAuth::user();
        $ToFollow = User::findOrFail($request->id);
        $validate = UserFollows::where('user_id','=',$user->id)->where('follow_id','=',$ToFollow->id)->first();
        
        if($validate!=null)
        {
            if($validate->status=='following')
            {
                DB::table('user_follows')
                ->where('user_id','=',$user->id)
                ->where('follow_id','=',$ToFollow->id
                )->update(['status' => 'unfollowing']);
                return $this->sendResponseOk('Unfollow',"OK");
            }
            if($validate->status=='unfollowing')
            {
                DB::table('user_follows')
                ->where('user_id','=',$user->id)
                ->where('follow_id','=',$ToFollow->id
                )->update(['status' => 'following']);
                return $this->sendResponseOk('Following',"OK");
            }

            if($validate->status=='ignored')
            {
                return $this->sendResponseOk('Ignored',"Ignored");
            }
        }
        $follower = new UserFollows;
        $follower->user_id = $user->id;
        $follower->follow_id = $ToFollow->id;
        $follower->status = 'following';
        $follower->save();

        return $this->sendResponseOk('Following',"OK");
    }

    public function Followers()
    {
        $user = JWTAuth::user();
        $followers = $user->Followers()->get();
        $thingsToReturn = new Collection;
        foreach ($followers as $follows) 
        {
            $finder = User::find($follows->follow_id);
            $thingsToReturn->push(['user_id'=>$finder->id,'avatar'=>$finder->avatar,'name'=>$finder->name]);
        }
        return $this->sendResponseOk($thingsToReturn,"OK");

    }

    public function GetUserRef($id)
    {
        $user = User::findOrFail($id);
        $validate = UserFollows::where('user_id','=',JWTAuth::user()->id)->where('follow_id','=',$user->id)->first();
            if($validate!=null) 
            {
                $user->follow_stat = 'Dejar de Seguir'; 
            }
            else
            {
                $user->follow_stat = 'Seguir'; 
            }
            
            $user->follows =  $user->Followers()->count();
            $user->followers =  $user->Followed()->count();
            $user->post =  $user->Posts()->count();

        return $this->sendResponseOk($user,"OK");        
    }

    public function UpdateAvatar(Request $request)
    {
        $user = JWTAuth::user();
        if($request->file('avatar'))
        {
        $path = Storage::disk('public')->putFile('avatars',$request->file('avatar'));
        $user->avatar = $path;
        }
        $user->name = $request->name;
        $user->save();
        
        return $this->sendResponseOk($user,"OK");
    }

    public function CreateUser(Request $request)
    {
        $validate = validator($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validate->fails()) return $this->sendResponseBadRequest($validate->errors()->first());
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        return $this->sendResponseCreated($user);
    }
    
    public function MyFollowers()
    {
        $user = JWTAuth::user();
        $followers =UserFollows::where('follow_id','=',$user->id)->get();
        $thingsToReturn = new Collection;
        if($followers->count()!=0)
        {
            foreach ($followers as $follows) 
            {
                $finder = User::find($follows->user_id);
                $thingsToReturn->push(['user_id'=>$finder->id,'avatar'=>$finder->avatar,'name'=>$finder->name]);
            }
            return $this->sendResponseOk($thingsToReturn,"OK");
        }
        else
        {
            return $this->sendResponseNoContent(); 
        }
        
    }
    
    public function UpdatePassword(Request $request)
    {
        $user = JWTAuth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        return $this->sendResponseCreated($user);
        
    }
    
}
